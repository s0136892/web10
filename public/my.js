$(document).on("ready", function() {
  $(".regular").slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            dots: true
          }
        }
    ]
  });

  $(".ajaxForm").submit(function(e){
      e.preventDefault();
      var href = $(this).attr("action");
      $.ajax({
        type: "POST",
        dataType: "json",
        url: href,
        data: $(this).serialize(),
        success: function(response){
        if(response.status == "success"){
          alert("We received your submission, thank you!");
        }else{
          alert("An error occured: " + response.message);
        }
      }
    });
  });

});

function clickButtom(){
  document.getElementById("InputName1").value="";
  document.getElementById("InputPhoneNumber1").value="";
  selectElement('region', 'default')
  document.getElementById("textareaNoResize").value="";
}

function fioInput(e){
  console.log(e.target.value)
  localStorage.setItem("FIO", e.target.value)
}

function phoneInput(e){
  console.log(e.target.value)
  localStorage.setItem("number", e.target.value)
}

function regionInput(e){
  console.log(e.target.value)
  localStorage.setItem("region", e.target.value)
}

function messageInput(e){
  console.log(e.target.value)
  localStorage.setItem("message", e.target.value)
}


function setFio(){
  if (localStorage.getItem("FIO") !== null){
    document.getElementById("InputName1").value=localStorage.getItem("FIO")
  }
}

function setPhone(){
  if (localStorage.getItem("number") !== null){
    document.getElementById("InputPhoneNumber1").value =localStorage.getItem("number")
  }
}

function setRegion(){
  if (localStorage.getItem("region") !== null){
    document.getElementById("region").value=localStorage.getItem("region")
  }
}

function setMessage(){
  if (localStorage.getItem("message") !== null){
    document.getElementById("textareaNoResize").value=localStorage.getItem("message")
  }
}


function selectElement(id, valueToSelect) {    
    let element = document.getElementById(id);
    element.value = valueToSelect;
}

setFio();
setPhone();
setRegion();
setMessage();

function myButton(){
  history.pushState({page: 2}, "title 2", "?page=2");
  exampleModalCenter.style.display = "flex";
  }
  
  //При клике вне окна
  window.onclick = function(event) {
  if (event.target == exampleModalCenter) {
    $('#exampleModalCenter').hide();
    }
  }
  
  addEventListener("popstate",function(){
      history.pushState({page: 1}, "title 1", "?page=1");
      $('#exampleModalCenter').hide();
      $("body").attr("style", "overflow: unset");
  }, true);

  function zakr(){
    $('exampleModalCenter').hide();
  }
 